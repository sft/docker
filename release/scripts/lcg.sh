
export PLATFORM=$(ls -1 /cvmfs/sft.cern.ch/lcg/releases/**/**/ | grep x86_64 | head -1)
export OS=$(echo $PLATFORM | cut -d'-' -f2)
export COMP=$(echo $PLATFORM | cut -d'-' -f3)

#Assume there is only one compiler for gcc and for clang
if [[ $COMP =~ "gcc"* ]]; then
    source /cvmfs/sft.cern.ch/lcg/contrib/gcc/**/**/setup.sh
elif [[ $COMP =~ "clang"* ]]; then
    source /cvmfs/sft.cern.ch/lcg/contrib/llvm/**/**/setup.sh
fi

source /cvmfs/sft.cern.ch/lcg/view/setup.sh

