FROM cern/cc7-base

LABEL maintainer="project-lcg-spi-internal@cern.ch"
ENV container docker

# Copy configuration files and the package list inside the container 
COPY misc/krb5.conf /etc/krb5.conf 
COPY builder/utility/eos.repo /etc/yum.repos.d/eos.repo
COPY builder/utility/packages.txt /tmp/packages

# Update packages and install all necessary tools; clean afterwards
RUN yum update -y \
    && yum install -y yum-plugin-ovl yum-plugin-priorities \
    && yum install -y $(cat /tmp/packages) \
    && rm -fv /tmp/packages \
    && yum clean all

# EOS configuration
RUN mkdir -p /eos /etc/sysconfig
COPY builder/utility/auto.eos /etc/auto.eos
COPY builder/utility/eos.user /etc/sysconfig/eos.user
COPY builder/utility/eos.project /etc/sysconfig/eos.project
RUN sed -i "s|browse_mode = no|browse_mode = yes|" /etc/autofs.conf \
    && echo "/eos /etc/auto.eos" >> /etc/auto.master

# Set the correct timezone
RUN ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# Add user sftnight, group sf and some subfolders in the $HOME folder
RUN groupadd -g 2735 sf \
    && useradd -u 14806 -ms /bin/bash sftnight \
    && usermod -g sf sftnight \
    && mkdir /home/sftnight/.ssh \
    && mkdir /home/sftnight/.ccache

# Set up ccache
RUN mkdir -p /ccache \
    && chown sftnight:sf /ccache

# Setup Bash configuration
COPY misc/bashrc /home/sftnight/.bashrc

# Setup SSH configuration for Jenkins
COPY misc/config /home/sftnight/.ssh/config
RUN chmod 600 /home/sftnight/.ssh/config \
    && chown -R sftnight:sf /home/sftnight/.ssh

# Create the build area
RUN mkdir -m 777 /workspace
WORKDIR /workspace

# Run bash as user sftnight as default command
USER sftnight
CMD ["/bin/bash"]
