#!/bin/bash

username=$1; shift
groupname=$1; shift


# Detect OS to set package manager
OS=$(lsb_release -si)

if [[ $OS == *CentOS* || $OS == *SLC* ]]; then
    pkg_cmd="yum"
elif [[ $OS == *Ubuntu* ]]; then
    pkg_cmd="apt-get"
elif [[ $OS == *Fedora* ]]; then
    pkg_cmd="dnf"
else
    echo "OS not found: ${OS}"
    exit 1
fi

$pkg_cmd install -y passwd 2> /dev/null

echo -e "\n\n [Setting user password]"
echo -e "\t Executing 'passwd $username'" 
passwd $username 

echo -e "\n\n [Changing owner of ~/.ssh]"
chown -R $username:$groupname `eval echo "~$username"`/.ssh

echo -e "\n\n [Generating ssh keys] - Press enter twice (leave in blank)"
ssh-keygen -t dsa -f /etc/ssh/ssh_host_dsa_key && ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key


# OS-specific post-build actions
if [ $OS == *CentOS* ]; then
    rm -rf /run/nologin
elif [ $OS == *Ubuntu* ]; then
    apt-get update  && apt-get -y -q install krb5-user libkrb5-dev
fi
