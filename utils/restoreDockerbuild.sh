#!/bin/sh

# Restore a docker image from the EOS repo and make it
# run with same parameters it was running in the original
# build node
#
# <javier.cervantes.villanueva@cern.ch>

# Parameters
# BUILD_NAME: folder name to download from eos, contains needed files to restore
#             the docker container

BUILD_NAME=$1
EOS_BUILD_URL=/eos/project/l/lcg/www/lcgpackages/dockerbuilds/$BUILD_NAME
BUILD_NUM=`echo $1 | cut -d"=" -f 5`

# Prepare EOS access
kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
export  EOS_MGM_URL=root://eosuser.cern.ch

# Import image directly from EOS
echo "Importing docker image from $BUILD_NAME..."
docker import ${EOS_BUILD_URL}/image.tar restored-build:$BUILD_NUM
IMAGE_NAME=restored-build:$BUILD_NUM

echo "Docker image: $IMAGE_NAME"

# Copy docker command to run the image with same parameters used in the
# original build node
cp ${EOS_BUILD_URL}/docker_run.sh .

# Replace IMAGE for the just downloaded image
sed -i "s/IMAGE/$IMAGE_NAME/" ./docker_run.sh

# run docker image
CONTAINER_ID=`source ./docker_run.sh`
docker exec -it $CONTAINER_ID bash
